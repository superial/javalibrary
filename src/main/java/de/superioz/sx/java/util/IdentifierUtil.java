package de.superioz.sx.java.util;

import java.util.Arrays;
import java.util.List;

/**
 * Created on 27.04.2016.
 */
public enum IdentifierUtil {

	;

	private final static Character[] digits = {
			'0', '1', '2', '3', '4', '5',
			'6', '7', '8', '9',
			'A', 'B', 'C', 'D', 'E', 'F',
			'G', 'H', 'I', 'J', 'K', 'L',
			'M', 'N', 'O', 'P', 'Q', 'R',
			'S', 'T', 'U', 'V', 'W', 'X',
			'Y', 'Z',
			'a', 'b', 'c', 'd', 'e', 'f',
			'g', 'h', 'i', 'j', 'k', 'l',
			'm', 'n', 'o', 'p', 'q', 'r',
			's', 't', 'u', 'v', 'w', 'x',
			'y', 'z'};

	/**
	 * Turns the integer into a string
	 *
	 * @param id The id
	 * @return The string
	 */
	public static String toString(int id, int length){
		String s = toString(id);

		while(s.length() < length){
			s = "0" + s;
		}
		return s;
	}

	/**
	 * Parse the string to an integer
	 *
	 * @param s The string
	 * @return The integer
	 */
	public static int parseInt(String s){
		int result = 0;

		List<Character> ch = Arrays.asList(digits);
		for(int i = s.length() - 1; i >= 0; i--){
			char c = s.charAt(i);
			int diff = s.length() - i;

			int r = ch.contains(c) ? ch.indexOf(c) : 0;
			int a = (int) (r * Math.pow(digits.length - diff, diff - 1));

			result += a;
		}
		return result;
	}

	/**
	 * Method inspired by the Integer class (changed a little bit for a greater radix)
	 *
	 * @param i The integer
	 * @return The string
	 */
	private static String toString(int i){
		int length = digits.length;
		char buf[] = new char[length];
		boolean negative = (i < 0);
		int charPos = length - 1;

		if(!negative){
			i = -i;
		}
		while(i <= -length){
			buf[charPos--] = digits[-(i % length)];
			i = i / length;
		}
		buf[charPos] = digits[-i];
		if(negative){
			buf[--charPos] = '-';
		}

		return new String(buf, charPos, (length - charPos));
	}


}
