package de.superioz.sx.java.util;

/**
 * Created on 05.03.2016.
 */
public interface Consumer<T> {

	/**
	 * I like Java 8 but if not here's a class for a consumer
	 * A custom method to handle with given object
	 *
	 * @param t The object
	 */
	void accept(T t);

}
