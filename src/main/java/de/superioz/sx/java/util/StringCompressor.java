package de.superioz.sx.java.util;

import java.io.*;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

/**
 * Created on 28.04.2016.
 */
public enum StringCompressor {

	;

	/**
	 * Compresses the text to a byte array
	 *
	 * @param text The text
	 * @return The byte array
	 */
	public static byte[] compress(String text){
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try{
			OutputStream out = new DeflaterOutputStream(baos);
			out.write(text.getBytes("UTF-8"));
			out.close();
		}
		catch(IOException e){
			throw new AssertionError(e);
		}
		return baos.toByteArray();
	}

	/**
	 * Decompresses a string
	 *
	 * @param bytes The compressed bytes
	 * @return The string
	 */
	public static String decompress(byte[] bytes){
		InputStream in = new InflaterInputStream(new ByteArrayInputStream(bytes));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try{
			byte[] buffer = new byte[8192];
			int len;
			while((len = in.read(buffer)) > 0){
				baos.write(buffer, 0, len);
			}
			return new String(baos.toByteArray(), "UTF-8");
		}
		catch(IOException e){
			throw new AssertionError(e);
		}
	}

}
