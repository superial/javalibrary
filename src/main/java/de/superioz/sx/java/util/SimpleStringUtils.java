package de.superioz.sx.java.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class was created as a part of SuperFramework
 *
 * @author Superioz
 */
public class SimpleStringUtils {

	private static final String IP_PATTERN = "(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])"
			+ "|([0-9a-f]{1,4}:){7}([0-9a-f]){1,4}|"
			+ "((http://|https://)?(www.)?(([a-zA-Z0-9-]){2,}\\.){1,4}([a-zA-Z]){2,6}(/([a-zA-Z-_/\\.0-9#:?=&;,]*)?)?)";

	/**
	 * Checks if given string contains an address
	 *
	 * @param message The string
	 * @return The result
	 */
	public static String containsAddress(String message){
		message = message.replaceAll("[^a-zA-Z0-9.\\s]", ".");

		Matcher m1 = Pattern.compile(IP_PATTERN, Pattern.CASE_INSENSITIVE).matcher(message);
		String matches = "";

		while(m1.find()){
			matches = m1.group(0);
		}
		return matches;
	}

	/**
	 * Uppers first letter of given normal string
	 *
	 * @param name The string
	 * @return The finished string
	 */
	public static String upperFirstLetter(String name){
		return name = name.substring(0, 1).toUpperCase() + name.substring(1);
	}

	/**
	 * Checks for blocked words
	 *
	 * @param message      The message
	 * @param blockedWords The blocked words list
	 * @return The list of blocked words as int
	 */
	public static String checkForBlockedWords(String message, List<String> blockedWords){
		String s = message.replace(" ", "").replace("[^a-zA-Z]", "").replace("ß", "ss");
		int counter = 0;

		// Check for blocked words
		for(String w : blockedWords){
			if(s.toLowerCase().contains(w)){
				return w;
			}
		}
		return "";
	}

	/**
	 * Checks how many letters are in caps
	 *
	 * @param message The message
	 * @return The result
	 */
	public static int getCapsInString(String message){
		int counter = 0;

		for(int i = 0; i < message.length(); i++){
			char x = message.charAt(i);
			if(Character.isUpperCase(x)){
				counter++;
			}
		}
		return counter;
	}

	/**
	 * Get the length of real characters in given message
	 *
	 * @param message The message
	 * @return The result
	 */
	public static int getRealLength(String message){
		int counter = 0;

		for(int i = 0; i < message.length(); i++){
			char x = message.charAt(i);
			if(Character.isLetter(x)){
				counter++;
			}
		}
		return counter;
	}

	/**
	 * Get how many words are in given string
	 *
	 * @param message   The message
	 * @param minLength The minimum word length
	 * @return The count
	 */
	public static int getWordCount(String message, int minLength){
		String[] splitted = message.split(" ");
		int counter = 0;

		for(String s : splitted){
			if(s.length() >= minLength)
				counter++;
		}

		return counter;
	}

	/**
	 * Makes the first letter upper case
	 *
	 * @param spacedName The name
	 * @param spacer     The spacer
	 * @return The finished string
	 */
	public static String upperFirstLetterSpaced(String spacedName, String spacer){
		if(!spacedName.contains(spacer)){
			return upperFirstLetter(spacedName);
		}

		String[] strings = spacedName.split(spacer);

		for(int i = 0; i < strings.length; i++){
			strings[i] = upperFirstLetter(strings[i]);
		}

		return ListUtil.insert(strings, " ");
	}

	/**
	 * Splits the given fullMessage (with given toReplace strings)
	 *
	 * @param fullMessage The fullMessage
	 * @param toReplace   The replacer
	 * @return The string array
	 */
	public static String[] multiSplit(String fullMessage, List<String> toReplace, char surrounder){
		// Regex
		String sur = "(" + surrounder + ")";
		String regex = sur + "(\\w+)" + sur;
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(fullMessage);
		List<String> newToReplace = new ArrayList<>();

		while(m.find()){
			String s = m.group(0);

			if(toReplace.contains(s)){
				newToReplace.add(s);
			}
		}

		List<String> parts = Arrays.asList(fullMessage.split(regex));
		String[] fullParts = new String[parts.size() + newToReplace.size()];
		int replaceCounter = 0;
		int partCounter = 0;

		for(int i = 0; i < fullParts.length; i++){
			if(i % 2 != 0){
				if(newToReplace.size() < replaceCounter + 1)
					continue;
				fullParts[i] = newToReplace.get(replaceCounter);
				replaceCounter++;
			}
			else{
				if(parts.size() < partCounter + 1)
					continue;
				fullParts[i] = parts.get(partCounter);
				partCounter++;
			}
		}
		return fullParts;
	}

	/**
	 * Checks if given string is an integer
	 *
	 * @param s The string
	 * @return The result
	 */
	public static boolean isInteger(String s){
		if(s.isEmpty()) return false;
		if(s.length() > (Integer.MAX_VALUE+"").length()) return false;
		for(int i = 0; i < s.length(); i++){
			if(i == 0 && s.charAt(i) == '-'){
				if(s.length() == 1) return false;
				else continue;
			}
			if(Character.digit(s.charAt(i), 10) < 0) return false;
		}
		return true;
	}

}
