package de.superioz.sx.java.util;

/**
 * Created on 30.03.2016.
 */
public interface Converter<I, O> {

	/**
	 * A converter to convert from one type into another
	 *
	 * @param val The input value
	 * @return The output value
	 */
	O convert(I val);

	I reverse(O val);

}
