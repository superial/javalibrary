package de.superioz.sx.java.util;

import lombok.Getter;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created on 29.03.2016.
 */
@Getter
public class WrappedFieldArray<T> {

	private List<Field> fields;
	private Object classInstance;
	private Converter converter;

	/**
	 * Constructor for this class
	 *
	 * @param fields List of fields
	 */
	public WrappedFieldArray(List<Field> fields, Object classInstance){
		this.fields = fields;
		this.classInstance = classInstance;
	}

	private WrappedFieldArray(Class<?> c, Class<?> type, Object classInstance, Converter converter){
		List<Field> fields = ReflectionUtils.getFields(type, c);

		if(fields.size() == 0){
			fields = ReflectionUtils.getFields(type, c, c.getSuperclass());
		}
		if(c.getEnclosingClass() != null)
			fields.addAll(ReflectionUtils.getFields(type, c.getEnclosingClass()));

		// Fields
		this.converter = converter;
		this.fields = fields;
		this.classInstance = classInstance;
	}

	public WrappedFieldArray(Object classInstance, Class<?> type, Converter converter){
		this(classInstance.getClass(), type, classInstance, converter);
	}

	public WrappedFieldArray(Object classInstance, Class<?> type){
		this(classInstance, type, null);
	}

	/**
	 * Length of this array
	 *
	 * @return The length as integer
	 */
	public int length(){
		return getFields().size();
	}

	/**
	 * Writes given value to field with given index
	 *
	 * @param index The index
	 * @param value The value
	 */
	public WrappedFieldArray write(int index, Object value){
		try{
			Field f = getField(index);
			f.setAccessible(true);

			Object newValue = value;
			if(getConverter() != null){
				newValue = getConverter().reverse(value);
			}
			f.set(getClassInstance(), newValue);
		}
		catch(IllegalAccessException e){
			throw new RuntimeException("Couldn't set field.");
		}
		return this;
	}

	/**
	 * Read field with given index
	 *
	 * @param index The index
	 * @return The value
	 */
	public Object read(int index){
		try{
			Field f = getField(index);
			f.setAccessible(true);
			T value = (T) f.get(getClassInstance());

			if(getConverter() != null){
				return getConverter().convert(value);
			}
			return value;
		}
		catch(IllegalAccessException e){
			throw new RuntimeException("Couldn't read field.");
		}
	}

	/**
	 * Gets the field with given index
	 *
	 * @param index The index
	 * @return The field
	 */
	public Field getField(int index){
		return fields.get(index);
	}

}
