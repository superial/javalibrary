package de.superioz.sx.java.util;

/**
 * Created on 25.04.2016.
 */
public enum BitMaskUtil {

	;

	/**
	 * Creates a bit mask with given flags
	 *
	 * @param flags The flags
	 * @return The mask
	 */
	public static byte create(byte... flags){
		return add((byte) 0, flags);
	}

	/**
	 * Adds flags to given mask
	 *
	 * @param mask  The mask
	 * @param flags The flags
	 * @return The result
	 */
	public static byte add(byte mask, byte... flags){
		byte newMask = mask;
		for(byte b : flags){
			newMask |= b;
		}
		return newMask;
	}

	/**
	 * Removes flags from given mask
	 *
	 * @param mask  The mask
	 * @param flags The flags
	 * @return The result
	 */
	public static byte remove(byte mask, byte... flags){
		byte newMask = mask;
		for(byte b : flags){
			newMask &= ~b;
		}
		return newMask;
	}

	/**
	 * Checks if given mask contains flag
	 *
	 * @param mask The mask
	 * @param flag The flag
	 * @return The result
	 */
	public static boolean contains(byte mask, byte flag){
		return (mask & flag) != 0;
	}

}
