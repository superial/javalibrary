package de.superioz.sx.java.util;

import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;

/**
 * This class was created as a part of SuperFramework
 *
 * @author Superioz
 */
public enum Base64Utils {

	;

	/**
	 * Encodes the given string
	 *
	 * @param s The string
	 * @return The base 64 encoded string
	 */
	public static String toBase64(String s){
		try{
			return DatatypeConverter.printBase64Binary(s.getBytes("UTF-8"));
		}
		catch(UnsupportedEncodingException e){
			return s;
		}
	}

	/**
	 * Decodes the given string
	 *
	 * @param s The string
	 * @return The base 64 decoded string
	 */
	public static String fromBase64(String s){
		try{
			return new String(DatatypeConverter.parseBase64Binary(s), "UTF-8");
		}
		catch(UnsupportedEncodingException e){
			return s;
		}
	}

//    /**
//     * A method to serialize an inventory to Base64 string.
//     *
//     * <p />
//     *
//     * Special thanks to Comphenix in the Bukkit forums or also known
//     * as aadnk on GitHub.
//     *
//     * <a href="https://gist.github.com/aadnk/8138186">Original Source</a>
//     *
//     * @param inventory to serialize
//     * @return Base64 string of the provided inventory
//     * @throws IllegalStateException
//     */
//    public static String inventoryToBase64(Inventory inventory) throws IllegalStateException {
//        try {
//            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);
//
//            // Write the size of the inventory
//            dataOutput.writeInt(inventory.getSize());
//
//            // Save every element in the list
//            for (int i = 0; i < inventory.getSize(); i++) {
//                dataOutput.writeObject(inventory.getItem(i));
//            }
//
//            // Serialize that array
//            dataOutput.close();
//            return Base64Coder.encodeLines(outputStream.toByteArray());
//        } catch (Exception e) {
//            throw new IllegalStateException("Unable to save item stacks.", e);
//        }
//    }
//
//    /**
//     *
//     * A method to get an {@link Inventory} from an encoded, Base64, string.
//     *
//     * <p />
//     *
//     * Special thanks to Comphenix in the Bukkit forums or also known
//     * as aadnk on GitHub.
//     *
//     * <a href="https://gist.github.com/aadnk/8138186">Original Source</a>
//     *
//     * @param data Base64 string of data containing an inventory.
//     * @return Inventory created from the Base64 string.
//     * @throws IOException
//     */
//    public static Inventory inventoryFromBase64(String data) throws IOException{
//        try {
//            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
//            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
//            Inventory inventory = Bukkit.getServer().createInventory(null, dataInput.readInt());
//
//            // Read the serialized inventory
//            for (int i = 0; i < inventory.getSize(); i++) {
//                inventory.setItem(i, (ItemStack) dataInput.readObject());
//            }
//
//            dataInput.close();
//            return inventory;
//        } catch (ClassNotFoundException e) {
//            throw new IOException("Unable to decode class type.", e);
//        }
//    }
//
//    /**
//     * Transforms the given array into a base64 format
//     */
//    public static String itemStackArrayToBase64(ItemStack[] items) throws IllegalStateException {
//        try {
//            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);
//
//            // Write the size of the inventory
//            dataOutput.writeInt(items.length);
//
//            // Save every element in the list
//            for (int i = 0; i < items.length; i++) {
//                dataOutput.writeObject(items[i]);
//            }
//
//            // Serialize that array
//            dataOutput.close();
//            return Base64Coder.encodeLines(outputStream.toByteArray());
//        } catch (Exception e) {
//            throw new IllegalStateException("Unable to save item stacks.", e);
//        }
//    }
//
//    /**
//     * Transforms given base64 into an itemstack array
//     */
//    public static ItemStack[] itemStackArrayFromBase64(String data) throws IOException {
//        try {
//            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
//            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
//            ItemStack[] items = new ItemStack[dataInput.readInt()];
//
//            // Read the serialized inventory
//            for (int i = 0; i < items.length; i++) {
//                items[i] = (ItemStack) dataInput.readObject();
//            }
//
//            dataInput.close();
//            return items;
//        } catch (ClassNotFoundException e) {
//            throw new IOException("Unable to decode class type.", e);
//        }
//    }


}
