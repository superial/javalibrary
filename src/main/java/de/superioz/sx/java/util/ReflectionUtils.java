package de.superioz.sx.java.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class created on April in 2015
 */
public enum ReflectionUtils {

	;

	/**
	 * Method to receive specific field from given class and class instance
	 *
	 * @param c         Class which declares the field
	 * @param instance  Instance of the class
	 * @param fieldName Name of the field
	 * @return Returns the object which the field contains
	 */
	public static Object getField(Class c, Object instance, String fieldName){
		try{
			Field f = c.getDeclaredField(fieldName);
			f.setAccessible(true);

			return f.get(instance);
		}
		catch(NoSuchFieldException | IllegalAccessException e){
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Get the fields in class hirarchy
	 *
	 * @param startClass      The start class
	 * @param exclusiveParent The exlusive parent
	 * @return The list of fields
	 */
	public static Iterable<Field> getFieldsUpTo(Class<?> startClass, Class<?> exclusiveParent){

		List<Field> currentClassFields = Arrays.asList(startClass.getDeclaredFields());
		Class<?> parentClass = startClass.getSuperclass();

		if(parentClass != null &&
				(exclusiveParent == null || !(parentClass.equals(exclusiveParent)))){
			List<Field> parentClassFields =
					(List<Field>) getFieldsUpTo(parentClass, exclusiveParent);
			currentClassFields.addAll(parentClassFields);
		}

		return currentClassFields;
	}

	/**
	 * Get fields from class
	 *
	 * @param classes The classes
	 * @param type    The type
	 * @return The list
	 */
	public static List<Field> getFields(Class<?> type, Class<?>... classes){
		List<Field> fields = new ArrayList<>();

		for(Class<?> c : classes){
			for(Field f : c.getDeclaredFields()){
				if(f.getType().equals(type))
					fields.add(f);
			}
		}

		return fields;
	}

	/**
	 * Get fields
	 *
	 * @param classes The classes
	 * @return The list
	 */
	public static List<Field> getFields(List<Class<?>> classes){
		List<Field> fields = new ArrayList<>();

		for(Class<?> c : classes){
			fields.addAll(Arrays.asList(c.getDeclaredFields()));
		}
		return fields;
	}

	/**
	 * Gets a declared field from the class
	 * The parameter is 'clazz' because 'class' is already
	 * used.
	 */
	public static Field getField(Class<?> clazz, String name){
		Object obj = null;
		try{
			Field f = clazz.getDeclaredField(name);
			f.setAccessible(true);
			obj = f;
		}
		catch(NoSuchFieldException ex){
			//
		}
		return (Field) obj;
	}

	/**
	 * Gets a method out of this class
	 */
	public static Method getMethod(Class<?> clazz, String name, Class<?>... args){
		try{
			for(Method m : clazz.getDeclaredMethods()){
				if(m.getName().equals(name) && classListEquals(args, m.getParameterTypes())){
					m.setAccessible(true);
					return m;
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return clazz.getMethods()[0];
	}

	/**
	 * Returns a method, that has the same name +
	 * the same parameterLength
	 */
	public static Method getMethodWith(Class<?> clazz, String name, int parameterLength){
		Object obj = null;
		try{
			for(Method m : clazz.getMethods()){
				if(m.getName().equals(name) && m.getParameterTypes().length == parameterLength){
					obj = m;
				}
			}
		}
		catch(Exception e){
			return null;
		}

		return (Method) obj;
	}

	/**
	 * Set a field.
	 */
	public static void setField(String field, Object obj, Object instance){
		Field f;
		try{
			f = instance.getClass().getDeclaredField(field);
			f.setAccessible(true);
			f.set(instance, obj);
		}
		catch(NoSuchFieldException | IllegalAccessException e){
			e.printStackTrace();
		}
	}

	/**
	 * Simply returns a class object
	 */
	public static Class<?> getClass(String name){
		try{
			return Class.forName(name);
		}
		catch(ClassNotFoundException e){
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Get constructor of given class with given parameter
	 *
	 * @param clazz          The class
	 * @param parameterTypes The parameter
	 * @return The constructor
	 */
	public static Constructor<?> getConstructor(Class<?> clazz, Class<?>... parameterTypes){
		for(Constructor<?> con : clazz.getConstructors()){
			if(classListEquals(con.getParameterTypes(), parameterTypes)){
				return con;
			}
		}
		return clazz.getConstructors()[0];
	}

	/**
	 * Instantiate given class with given objects
	 *
	 * @param clazz     The class
	 * @param arguments The objects
	 * @return The inits object
	 */
	public static Object instantiateObject(Class<?> clazz, Object... arguments){
		try{
			return getConstructor(clazz, getClasses(arguments)).newInstance(arguments);
		}
		catch(InstantiationException | IllegalAccessException | InvocationTargetException e){
			e.printStackTrace();
		}
		return null;
	}

	public static Object instantiateObject(Class<?> clazz, Object[] parameterTypes, Object... arguments){
		try{
			return getConstructor(clazz, getClasses(parameterTypes)).newInstance(arguments);
		}
		catch(InstantiationException | IllegalAccessException | InvocationTargetException e){
			e.printStackTrace();
		}
		return null;
	}

	public static Object instantiateObject(String className, Object... arguments){
		return instantiateObject(getClass(className), arguments);
	}

	/**
	 * Get the class of given types
	 *
	 * @param arguments The arguments
	 * @return The classes array
	 */
	public static Class<?>[] getClasses(Object... arguments){
		Class<?>[] arr = new Class[arguments.length];

		for(int i = 0; i < arguments.length; i++){
			arr[i] = arguments[i].getClass();
		}
		return arr;
	}

	/**
	 * Invoke an object into a method
	 *
	 * @param instance   The object instance
	 * @param methodName The method name
	 * @param arguments  The arguments
	 * @return The returned object
	 */
	public static Object invokeMethod(Object instance, String methodName, Object... arguments){
		try{
			return getMethod(instance.getClass(), methodName).invoke(instance, arguments);
		}
		catch(IllegalAccessException | InvocationTargetException e){
			e.printStackTrace();
		}
		return instance;
	}

	/**
	 * Invokes the method fast
	 *
	 * @param instance   The instance
	 * @param methodName The name
	 * @param arguments  The arguments
	 * @return The object
	 */
	public static Object fastMethodInvoking(Object instance, String methodName,
	                                        Class<?>[] types, Object... arguments){
		try{
			Method m = instance.getClass().getDeclaredMethod(methodName, types);
			return m.invoke(instance, arguments);
		}
		catch(NoSuchMethodException | InvocationTargetException | IllegalAccessException e){
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Checks if first class list contains other
	 */
	public static boolean classListEquals(Class<?>[] l1, Class<?>[] l2){
		boolean equal = true;

		if(l1.length != l2.length)
			return false;
		for(int i = 0; i < l1.length; i++){
			if(l1[i] != l2[i]){
				equal = false;
				break;
			}
		}

		return equal;
	}

}
