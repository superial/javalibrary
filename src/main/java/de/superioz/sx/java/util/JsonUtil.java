package de.superioz.sx.java.util;

import com.google.gson.Gson;

/**
 * Created on 27.04.2016.
 */
public enum JsonUtil {

	;

	private static Gson gson = new Gson();

	/**
	 * Returns the gson sx
	 *
	 * @return The gson sx object
	 */
	public static Gson getGson(){
		return gson;
	}

}
