package de.superioz.sx.java.file;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class created on April in 2015
 */
public class TextFile extends CustomFile {

	/**
	 * Constructor of kind file
	 *
	 * @param filename Name of file {@link super#filename}
	 */
	public TextFile(String filename, String extraPath, File root){
		super(filename, extraPath, root, SupportedFiletype.TEXT);
	}

	/**
	 * Writes to {@link super#file} WITHOUT auto backslash
	 *
	 * @param strings The strings to write into
	 * @see PrintWriter#print(String)
	 */
	public void write(String... strings){
		try{
			PrintWriter writer = new PrintWriter(super.file, "UTF-8");

			for(String s : strings){
				writer.print(s);
			}
			writer.close();
		}
		catch(FileNotFoundException
				| UnsupportedEncodingException e){
			e.printStackTrace();
		}
	}

	/**
	 * Writes to {@link super#file} WITH auto backslash
	 *
	 * @param strings The strings to write into
	 * @see PrintWriter#println(String)
	 */
	public void writeln(String... strings){
		try{
			PrintWriter writer = new PrintWriter(super.file, "UTF-8");

			for(String s : strings){
				writer.println(s);
			}
			writer.close();
		}
		catch(FileNotFoundException
				| UnsupportedEncodingException e){
			e.printStackTrace();
		}
	}

	/**
	 * Read all lines from file
	 *
	 * @return The list of text
	 */
	public List<String> readln(){
		List<String> lines = new ArrayList<>();

		try{
			BufferedReader br = new BufferedReader(
					new InputStreamReader(new FileInputStream(file), "UTF-8"));

			for(String line; (line = br.readLine()) != null;){
				lines.add(line);
			}
			br.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		return lines;
	}

}
