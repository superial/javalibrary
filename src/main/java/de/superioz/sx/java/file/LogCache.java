package de.superioz.sx.java.file;

import de.superioz.sx.java.util.TimeUtils;
import lombok.Getter;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class created on April in 2015
 */
@Getter
public class LogCache {

    protected List<String> lines;
    protected long startTimestamp;
    protected long endTimestamp;
    protected String folder;
    protected File dataFolder;
	protected String fileName;

    public LogCache(File dataFolder, String folder, String fileName){
        this.lines = new ArrayList<>();
	    this.dataFolder = dataFolder;
	    this.fileName = fileName;
        this.folder = folder;

        if(!folder.isEmpty()){
            this.folder = "/" + folder;
        }

        startTimestamp = TimeUtils.timestamp();
        lines.add("# Start logfile @" + startTimestamp);
    }

    /**
     * Only important for me tho
     */
    public LogCache log(String msg){
        lines.add("[" + TimeUtils.getCurrentTime() + "]: " + msg);
        return this;
    }

    /**
     * Only important for me tho
     */
    public File build(){
        endTimestamp = TimeUtils.timestamp();

        String fileName = getFileName().replace("%time", TimeUtils.getCurrentTime("dd-MM-YYYY_HH-mm-ss"));
        File f = new File(dataFolder + folder, fileName + ".log");

        if(!f.exists()){
            f.getParentFile().mkdirs();
            try{
                f.createNewFile();
            }catch(IOException e){
                e.printStackTrace();
            }
        }

        // Set text of file
        try{
            final PrintWriter writer = new PrintWriter(f, "UTF-8");

            for(String l : lines){
                writer.println(l);
            }

            writer.println("# End logfile @" + endTimestamp);
            writer.close();
        }catch(FileNotFoundException
                | UnsupportedEncodingException e){
            e.printStackTrace();
        }
        return f;
    }

    /**
     * Only important for me tho
     */
    public long end(){
        return endTimestamp;
    }

    /**
     * Only important for me tho
     */
    public long start(){
        return startTimestamp;
    }

}
