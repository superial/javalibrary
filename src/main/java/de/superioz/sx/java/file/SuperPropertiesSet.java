package de.superioz.sx.java.file;

import lombok.Getter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 28.04.2016.
 */
@Getter
public class SuperPropertiesSet<E> {

	private List<SuperProperties<E>> propertyList;

	public SuperPropertiesSet(){
		this.propertyList = new ArrayList<>();
	}


	/**
	 * Adds a property file to the property list
	 *
	 * @param properties The property
	 * @param s1         The resource extra path (leave blank for none)
	 * @param f1         Should the file directly be created?
	 */
	public void add(SuperProperties<E> properties, String s1, boolean f1){
		properties.load(s1, true, f1);

		this.propertyList.add(properties);
	}

	public void add(String filename, String extraPath, File root,
	                String s1, boolean f1){
		this.add(new SuperProperties<E>(filename, extraPath, root), s1, f1);
	}

	/**
	 * Queries an object from given key
	 *
	 * @param key The key
	 * @return The result
	 */
	public E q(String key){
		E obj = null;
		for(SuperProperties<E> pro : propertyList){
			obj = pro.get(key);
		}
		return obj;
	}

	/**
	 * Writes data into given file
	 *
	 * @param key      The key
	 * @param obj      The object
	 * @param fileName The file name
	 */
	public void w(String key, E obj, String fileName){
		if(!contains(fileName)){
			return;
		}
		SuperProperties<E> pro = get(fileName);
		pro.set(key, obj);
	}

	/**
	 * Removes a property file
	 *
	 * @param filename The filename
	 */
	public void remove(String filename){
		if(contains(filename)){
			SuperProperties<E> pro = get(filename);
			propertyList.remove(pro);
		}
	}

	/**
	 * Gets a property file from given name
	 *
	 * @param fileName The name
	 * @return The properties object
	 */
	public SuperProperties<E> get(String fileName){
		for(SuperProperties<E> pro : propertyList){
			if(pro.getFilename().equals(fileName)){
				return pro;
			}
		}
		return null;
	}

	/**
	 * Checks if given file already exists
	 *
	 * @param fileName The fileName
	 * @return The result
	 */
	public boolean contains(String fileName){
		return get(fileName) != null;
	}

}
