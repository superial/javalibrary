package de.superioz.sx.java.file;

import java.io.File;

/**
 * This class was created as a part of GunGame (Spigot)
 *
 * @author Superioz
 */
class PropertiesFile extends CustomFile {

    /**
     * Constructor of this fileWrapper
     *
     * @param filename  {@link #filename}
     * @param extraPath Extrapath from the plugin folder like "folder/extra/path"
     */
    public PropertiesFile(String filename, String extraPath, File root){
        super(filename, extraPath, root, SupportedFiletype.PROPERTIES);
    }

}
