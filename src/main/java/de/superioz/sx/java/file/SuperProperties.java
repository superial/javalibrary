package de.superioz.sx.java.file;

import lombok.Getter;

import java.io.*;
import java.util.Properties;

/**
 * This class was created as a part of SuperLibrary
 *
 * @author Superioz
 */
@Getter
public class SuperProperties<E> extends PropertiesFile {

	private Properties handle;
	private PropertyFilter filter;

	public SuperProperties(String filename, String extraPath, File root){
		super(filename, extraPath, root);
		handle = new Properties();
	}

	@Override
	public void load(String resourcePath, boolean copyDefaults, boolean create){
		super.load(resourcePath, copyDefaults, create);

		try{
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(
							new FileInputStream(super.getFile()), "UTF-8"));
			this.handle.load(reader);
			reader.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}

	@Override
	public void load(boolean copyDefaults, boolean create){
		this.load("", copyDefaults, create);
	}

	/**
	 * Gets object from given key
	 *
	 * @param key The key
	 * @return The object
	 */
	@SuppressWarnings("unchecked")
	public E get(String key){
		if(filter != null)
			return (E) filter.filter(handle.get(key));
		else
			return (E) handle.get(key);
	}

	/**
	 * Set object to file with given key
	 *
	 * @param key    The key
	 * @param object The object
	 */
	public void set(String key, E object){
		handle.setProperty(key, String.valueOf(object));
	}

	/**
	 * Applies filter to this file
	 *
	 * @param filter The filter
	 */
	public void applyFilter(PropertyFilter<E> filter){
		this.filter = filter;
	}

}
