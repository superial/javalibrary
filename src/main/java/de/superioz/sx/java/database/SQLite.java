package de.superioz.sx.java.database;


import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Connects to and uses a SQLite database
 *
 * @author tips48
 */
public class SQLite extends Database {

    private final String dbLocation;
    private final File dataFolder;

    /**
     * Creates a new SQLite instance
     *
     * @param dataFolder Folder
     * @param dbLocation Location of the Database (Must end in .db)
     */
    public SQLite(File dataFolder, String dbLocation){
        this.dbLocation = dbLocation + ".db";
	    this.dataFolder = dataFolder;
    }

    // -- Intern methods

    @Override
    public Connection openConnection(){
        if(checkConnection()){
            return connection;
        }
        if(!getDataFolder().exists()){
            getDataFolder().mkdirs();
        }
        File file = new File(getDataFolder(), dbLocation);
        if(!(file.exists())){
            try{
                file.createNewFile();
            }catch(IOException e){
	            System.err.println("Unable to create database!");
            }
        }

        try{
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager
                    .getConnection("jdbc:sqlite:"
                            + getDataFolder().toPath().toString() + "/"
                            + dbLocation);
            return connection;
        }catch(SQLException | ClassNotFoundException e){
            //
        }

        return null;
    }

	// -- Intern methods

    public String getDbLocation(){
        return dbLocation;
    }

	public File getDataFolder(){
		return dataFolder;
	}
}
