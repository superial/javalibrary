package de.superioz.sx.java.database;

import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collections;

/**
 * This class was created as a part of SuperLibrary
 *
 * @author Superioz
 */
public class MongoDB {

	private MongoClient client;
	private MongoDatabase database;

	private ServerAddress serverAddress;
	private MongoCredential authentification;

	public static final int DEFAULT_PORT = 27017;
	public static final int DEFAULT_SHARD_PORT = 27018;
	public static final int DEFAULT_CONFIG_PORT = 27019;
	public static final int DEFAULT_WEBSTATUS_PORT = 28017;

	public MongoDB(String serverAddress, int port, String username, String password, String database){
		try{
			InetAddress address = InetAddress.getByName(serverAddress);
			this.serverAddress = new ServerAddress(address, port);
		}catch(UnknownHostException ex){
			System.err.println("Couldn't connect to MongoDB!");
			return;
		}

		// Check credentials
		if(!username.isEmpty()){
			authentification = MongoCredential.createCredential(username, database, password.toCharArray());
			this.client = new MongoClient(this.serverAddress, Collections.singletonList(authentification),
					new MongoClientOptions.Builder().serverSelectionTimeout(5000).build());
		}
		else{
			this.client = new MongoClient(this.serverAddress);
		}
		this.connect(database);
	}

	public MongoDB(String serverAddress, int port, String database){
		this(serverAddress, port, "", "", database);
	}

	/**
	 * Connect to given database
	 *
	 * @param database The database name
	 */
	public void connect(String database){
		this.database = this.client.getDatabase(database);
	}

	/**
	 * Creates a collection with given name
	 *
	 * @param coll The name
	 */
	public void createCollection(String coll){
		if(!collectionExists(coll))
			getDatabase().createCollection(coll);
	}

	/**
	 * Deletes the collection with given name
	 *
	 * @param coll The name
	 */
	public void deleteCollection(String coll){
		if(collectionExists(coll))
			getDatabase().getCollection(coll).drop();
	}

	/**
	 * Checks if given collection exists
	 *
	 * @param name The name
	 * @return The result
	 */
	public boolean collectionExists(String name){
		for(final String n : getDatabase().listCollectionNames()){
			if(n.equals(name)){
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the collection with given name
	 *
	 * @param collection The name
	 * @return The collection
	 */
	public MongoCollection getCollection(String collection){
		if(!collectionExists(collection)){
			createCollection(collection);
		}
		return getDatabase().getCollection(collection);
	}

	/**
	 * Check the connection to the server
	 *
	 * @return The result
	 */
	public boolean checkConnection(){
		try{
			getDatabase().runCommand(new Document("dbStats", 1));
			return true;
		}
		catch(Exception ex){
			return false;
		}
	}

	// -- Intern methods

	public MongoDatabase getDatabase(){
		return this.database;
	}

	public MongoClient getClient(){
		return this.client;
	}

	public String getDatabaseName(){
		return getDatabase().getName();
	}

	public String getServerAddress(){
		return serverAddress.getHost();
	}

	public int getServerPort(){
		return serverAddress.getPort();
	}

	public MongoCredential getAuthentification(){
		return authentification;
	}

}
